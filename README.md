# MassiveBox's Homepage

A simple HTML, CSS and JQuery static homepage built with Bulma.  

Don't forget to clone the repo with `--recurse-submodules` to get all dependancies:
- Bulma for the base CSS of the website
- ForkAwesome for awesome FOSS icons
- jQuery because plain JS is boring
- RSS Parser because Pleroma's feed is in RSS format

You can check out a live version at https://massivebox.net or https://massivebox.netlify.app/ (without the Pleroma feed since the path is relative, so it tries to fetch https://massivebox.netlify.app/feed.atom, which doesn't exist).